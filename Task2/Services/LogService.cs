﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Task2.Enums;
using Task2.Models;

namespace Task2.Services
{
    public sealed class LogService
    {
        private readonly ApplicationContext _context;

        public LogService(ApplicationContext context)
        {
            _context = context;
        }

        public int AddUserAction(UserActionType userActionType, string message)
        {
            var userAction = new UserAction()
            {
                DateTime = DateTime.Now,
                ActionType = userActionType,
                Message = message
            };

            _context.Logs.Add(userAction);
            _context.SaveChanges();
            return userAction.Id;
        }

        public async Task<int> AddUserActionAsync(UserActionType userActionType, string message)
        {
            var userAction = new UserAction()
            {
                DateTime = DateTime.Now,
                ActionType = userActionType,
                Message = message
            };

            _context.Logs.Add(userAction);
            await  _context.SaveChangesAsync();
            return userAction.Id;
        }
    }
}
