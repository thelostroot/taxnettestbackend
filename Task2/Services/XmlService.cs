﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Task2.Models;

namespace Task2.Services
{
    public class XmlService
    {
        
        public bool CheckFileName(string fileName)
        {
            string name = Path.GetFileNameWithoutExtension(fileName);
            string extensions = Path.GetExtension(fileName);

            bool result = true;

            string pattern = @"^[А-ЯЁа-яё]{1,100}_([0-9]{1,10}|[0-9]{14,20})_.{1,7}$";
            Regex regex = new Regex(pattern);
            result = regex.IsMatch(name);

            if (extensions != ".xml")
                result = false;

            return result;
        }

        public FileRecord DeserializationXml(string xml)
        {
            XDocument xDoc = XDocument.Parse(xml);
            return new FileRecord()
            {
                Id = 0,
                Version = xDoc.Element("File").Attribute("FileVersion").Value,
                Name = xDoc.Element("File").Element("Name").Value,
                EditTime = DateTime.Parse(xDoc.Element("File").Element("EditTime").Value)
            };
        }

        public string SerializationToXml(List<FileRecord> fileRecords)
        {
            if (fileRecords.Count == 1)
                return SerializeFileRecord(fileRecords.First());
            else
                return SerializeFileRecords(fileRecords);
        }

        private string SerializeFileRecord(FileRecord fileRecord)
        {
            XDocument xDoc = new XDocument();
            XElement file = new XElement("File");
            file.Add(new XAttribute("FileVersion", fileRecord.Version));

            XElement name = new XElement("Name") {Value = fileRecord.Name};
            XElement editTime = new XElement("EditTime") {Value = fileRecord.EditTime.ToString()};
            file.Add(name);
            file.Add(editTime);
            xDoc.Add(file);

            return xDoc.ToString();
        }

        private string SerializeFileRecords(List<FileRecord> fileRecords)
        {
            XDocument xDoc = new XDocument();
            XElement files = new XElement("Files");

            foreach (var fileRecord in fileRecords)
            {
                XElement file = new XElement("File");
                file.Add(new XAttribute("FileVersion", fileRecord.Version));

                XElement name = new XElement("Name") { Value = fileRecord.Name };
                XElement editTime = new XElement("EditTime") { Value = fileRecord.EditTime.ToString() };
                file.Add(name);
                file.Add(editTime);
                files.Add(file);
            }

            xDoc.Add(files);

            return xDoc.ToString();
        }
    }
}
