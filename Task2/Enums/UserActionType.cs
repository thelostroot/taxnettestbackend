﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task2.Enums
{
    public enum UserActionType { VisitToDashboard, AddFileRecord, UpdateFileRecord, RemoveFileRecord, UploadXml, DownloadXml}
}
