﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task2.Models;

namespace Task2
{
    public class ApplicationContext : DbContext
    {
        public DbSet<FileRecord> FileRecords { get; set; }
        public DbSet<UserAction> Logs { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
        }
    }
}
