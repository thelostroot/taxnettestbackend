﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Task2.Enums;
using Task2.Services;

namespace Task2.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationContext _context;

        public HomeController(ApplicationContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            var logService = new LogService(_context);
            logService.AddUserAction(UserActionType.VisitToDashboard, "Вход на главную страницу приложения");
            return View();
        }
    }
}
