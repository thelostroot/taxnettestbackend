﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task2;
using Task2.Enums;
using Task2.Models;
using Task2.Services;

namespace Task2.Controllers
{
    [Produces("application/json")]
    [Route("api/FileRecords")]
    public class FileRecordsController : Controller
    {
        private readonly ApplicationContext _context;

        public FileRecordsController(ApplicationContext context)
        {
            _context = context;
        }

        // GET: api/Files
        [HttpGet]
        public IEnumerable<FileRecord> GetFileRecords()
        {
            return _context.FileRecords;
        }

        // GET: api/Files/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFileRecord([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var fileRecord = await _context.FileRecords.SingleOrDefaultAsync(m => m.Id == id);

            if (fileRecord == null)
            {
                return NotFound();
            }

            return Ok(fileRecord);
        }

        // PUT: api/Files/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFileRecord([FromRoute] int id, [FromBody] FileRecord fileRecord)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != fileRecord.Id)
            {
                return BadRequest();
            }

            var oldFileRecord = await _context.FileRecords.FirstOrDefaultAsync(x => x.Id == id);
            var oldState = oldFileRecord.ToString();

            oldFileRecord.Name = fileRecord.Name;
            oldFileRecord.EditTime = fileRecord.EditTime;
            oldFileRecord.Version = fileRecord.Version;

            _context.Entry(oldFileRecord).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();

                var logService = new LogService(_context);
                await logService.AddUserActionAsync(UserActionType.UpdateFileRecord,
                    $"{oldState} -----> {fileRecord.ToString()}");
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FileExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(fileRecord);
        }

        // POST: api/Files
        [HttpPost]
        public async Task<IActionResult> PostFileRecord([FromBody] FileRecord fileRecord)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.FileRecords.Add(fileRecord);
            await _context.SaveChangesAsync();

            var logService = new LogService(_context);
            await logService.AddUserActionAsync(UserActionType.AddFileRecord,
                $"{fileRecord}");

            return CreatedAtAction("GetFileRecord", new { id = fileRecord.Id }, fileRecord);
        }

        // DELETE: api/Files/5
        [HttpDelete]
        public async Task<IActionResult> DeleteFileRecord(int[] ids)
        { 
            _context.FileRecords.RemoveRange(_context.FileRecords.Where(x => ids.Contains(x.Id)));
            await _context.SaveChangesAsync();

            var logService = new LogService(_context);
            await logService.AddUserActionAsync(UserActionType.RemoveFileRecord,
                $"Remove id: {string.Join(" ", ids)}");

            return Ok(ids);
        }

        private bool FileExists(int id)
        {
            return _context.FileRecords.Any(e => e.Id == id);
        }
    }
}