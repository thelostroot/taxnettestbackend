﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task2;
using Task2.Models;

namespace Task2.Controllers
{
    [Produces("application/json")]
    [Route("api/Logs")]
    public class LogsController : Controller
    {
        private readonly ApplicationContext _context;

        public LogsController(ApplicationContext context)
        {
            _context = context;
        }

        // GET: api/Logs
        [HttpGet]
        public IEnumerable<UserAction> GetLogs()
        {
            return _context.Logs.OrderByDescending(x => x.Id);
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteAll()
        {
            // ток для маленьких таблиц
            _context.Logs.RemoveRange(_context.Logs);
            await _context.SaveChangesAsync();

            return Ok();
        }
    }
}