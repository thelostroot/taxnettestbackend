﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task2.Enums;
using Task2.Models;
using Task2.Services;

namespace Task2.Controllers
{
    [Produces("application/json")]
    [Route("api/Xml")]
    public class XmlController : Controller
    {
        private readonly ApplicationContext _context;

        public XmlController(ApplicationContext context)
        {
            _context = context;
        }

        [HttpPost("UploadFile")]
        public async Task<IActionResult> UploadFile()
        {
            if (Request.Form.Files.Count > 0)
            {
                var logService = new LogService(_context);
                var xmlService = new XmlService();

                if (!xmlService.CheckFileName(Request.Form.Files.First().FileName))
                {
                    await logService.AddUserActionAsync(UserActionType.UploadXml,
                        $"Имя файла: {Request.Form.Files.First().FileName} Имя файла не соответсвует шаблону!");
                    return BadRequest("Имя файла не соответсвует шаблону!");
                }
                    

                StreamReader reader = new StreamReader(Request.Form.Files[0].OpenReadStream(), Encoding.UTF8);
                string fileText = await reader.ReadToEndAsync();
               
                try
                {
                    var fileRecord = xmlService.DeserializationXml(fileText);
                    _context.FileRecords.Add(fileRecord);
                    await _context.SaveChangesAsync();
                    
                    await logService.AddUserActionAsync(UserActionType.UploadXml,
                        $"Имя файла: {Request.Form.Files.First().FileName}\n Запись ---> {fileRecord}");

                    return Ok(fileRecord);
                }
                catch (Exception e)
                {
                    await logService.AddUserActionAsync(UserActionType.UploadXml,
                        $"Имя файла: {Request.Form.Files.First().FileName} Не удалось распарсить файл!");

                    return BadRequest("Не удалось спарсить файл!");
                }
            }

            return BadRequest("Файл не найден!");
        }

        [HttpGet("Download")]
        public async Task<IActionResult> Download(int[] ids)
        {
            var logService = new LogService(_context);
            var fileRecords = new List<FileRecord>();

            foreach (var id in ids)
            {
                var fileRecord = await _context.FileRecords.FirstOrDefaultAsync(x => x.Id == id);
                if (fileRecord == null)
                    return BadRequest($"File record with id {id} not found!");
                fileRecords.Add(fileRecord);
            }

            var xmlService = new XmlService();
            var xmlString = xmlService.SerializationToXml(fileRecords);

            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(xmlString);
            writer.Flush();
            stream.Position = 0;

            await logService.AddUserActionAsync(UserActionType.UploadXml,
                $"Id Записей: {string.Join(" ", ids)}");

            return File(stream, "text/html", "file_records.xml");
        }
    }
}