﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Task2.Models
{
    public class FileRecord
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public String Name { get; set; }

        [Required]
        public String Version { get; set; }

        [Required]
        public DateTime EditTime { get; set; }

        public override string ToString()
        {
            return $"Id:{Id} Name:{Name} Version:{Version} EditTime:{EditTime}";
        }
    }
}
