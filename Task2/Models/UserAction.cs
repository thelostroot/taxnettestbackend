﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Task2.Enums;

namespace Task2.Models
{
    public class UserAction
    {
        public int Id { get; set; }

        [Required]
        public DateTime DateTime { get; set; }

        [Required]
        public UserActionType ActionType { get; set; }

        [Required]
        public string Message { get; set; }
    }
}
